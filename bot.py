import os, sys, json, tweepy, markovify, string
from apscheduler.schedulers.blocking import BlockingScheduler
from secrets import *

def get_random_text():
    text_model = markovify.NewlineText("\n".join(corpus))
    return text_model.make_short_sentence(140)


def tweet_random():
    text = get_random_text()
    if text:
        api.update_status(get_random_text())

def feed_corpus():
    tweets = api.search(list(string.ascii_lowercase), lang="en", rpp=100)
    corpus.extend([tweet for tweet in tweets if "@" not in tweet.text and "RT" not in tweet.text and "\n" not in tweet.text and "https" not in tweet.text])
    if len(corpus) > 2000:
        corpus = corpus[-2000:]

if __name__ == "__main__":
    auth = tweepy.OAuthHandler(C_KEY, C_SECRET)
    auth.set_access_token(A_TOKEN, A_TOKEN_SECRET)
    api = tweepy.API(auth)

    corpus = []

    scheduler = BlockingScheduler()
    scheduler.add_job(tweet_random, 'cron', minute="*/15")
    scheduler.add_job(feed_corpus, 'cron', minute="*/5")
    scheduler.start()
