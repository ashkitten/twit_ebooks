#!/bin/sh

if [ $1 == "start" ]; then
	echo "Starting..."
    daemon -f -r -p .bot.pid -P .bot_daemon.pid python3 bot.py
elif [ $1 == "restart" ]; then
    if [ -f .bot_daemon.pid ]; then
		echo "Restarting..."
        # The supervisor will automatically restart the process
        pkill -F .bot.pid
    else
        echo "Process is not running"
    fi
elif [ $1 == "stop" ]; then
	echo "Stopping..."
    pkill -F .bot_daemon.pid
elif [ $1 == "test" ]; then
    python3 test.py
fi
